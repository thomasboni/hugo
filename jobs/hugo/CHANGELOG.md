# Changelog

## [0.3.0]

* Downgrade alpine version to `3.14.3`

## [0.2.0]

* Fix job syntax
* Use alpine image

## [0.1.0]

* Initial version
