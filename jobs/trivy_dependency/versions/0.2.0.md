* Add ability to exit on a particular severity
* Add possibility to append options for command `trivy`
* Change default value for `TRIVY_EXIT_CODE`